#!/bin/bash

# 將 Django 的靜態檔蒐集至 static_root（定義在 settings.py 中）
echo "collect static files............................."
python /package/webapi/manage.py collectstatic


echo "nginx set daemon on............................."
# 使用 daemon on; 必須要用 '' 包住
# daemon on; 會將 nginx 放在背景中運行，不設為 daemon off; 是因為 django log 我目前只會讓它顯示在 uwsgi，所以要讓 uwsgi 在外面運行
nginx -g 'daemon on;'


echo "use uwsgi run django............................."
# 需加 -d 可設為背景運行，但目前不需要
uwsgi --ini uwsgi.ini


