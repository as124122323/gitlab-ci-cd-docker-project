from django.http import JsonResponse
from rest_framework import views
from datetime import datetime
import logging

logger = logging.getLogger(__name__)

class ShowCurrentTimeView(views.APIView):
    def get(self, request, *args, **kwargs):
        logger.info("request.path: %s" % request.path)
        try:
            data = datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')
        except Exception as e:
            logger.error(e)
            return JsonResponse({
                "status": "failed", 
                "result": None, 
                "error_msg": e
            })
        return JsonResponse({
            "status": "success", 
            "result": data, 
            "error_msg": None
        })