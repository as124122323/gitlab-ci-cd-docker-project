from django.urls import path
from . import views

urlpatterns = [
    path('show_time/', views.ShowCurrentTimeView.as_view(), name='show_time'),
]