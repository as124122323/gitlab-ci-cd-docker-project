# 拉取 Python Image
FROM python:3.9

# 設定工作目錄，如果該目錄不存在的話會自動建立
# 必須將 WORKDIR 設為 Django 根目錄，不然之後用 uwsgi 運行 web 後，並訪問 web 時會報 500 error
WORKDIR /package/webapi

# 將設定檔複製至對應位置
COPY ["build_config", "/package/build_config"]

RUN apt-get update && \
    apt-get -y install nginx curl

RUN rm /etc/nginx/sites-enabled/default

# sites-available 目錄（放我們的 conf 檔）
# sites-enabled 目錄（透過 ln 將需要的 conf 檔從 sites-available 軟連結過來）
RUN cp /package/build_config/nginx/* /etc/nginx/sites-available && \
    ln -s /etc/nginx/sites-available/web.conf /etc/nginx/sites-enabled/

# 安裝 python 套件
RUN pip install -r /package/build_config/python/requirements.txt

# 將 Django 專案複製進容器
COPY ["www/webapi", "/package/webapi"]
RUN chmod -R 777 /package

ENTRYPOINT ["/bin/bash", "/package/build_config/python/entrypoint.sh"]